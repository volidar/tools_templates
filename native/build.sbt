addCommandAlias("ep", "~exported-products")
addCommandAlias("ept", "~;exported-products;test:exported-products")
addCommandAlias("crep", ";clean;reload;exported-products")


name := """native"""

version := "1.0"

val zScalaVersion = "2.11.8"

scalaVersion := zScalaVersion

resolvers += Resolver.sonatypeRepo("snapshots")

libraryDependencies ++= Seq.empty

scalacOptions in ThisBuild ++= Seq(
  //"-missing-interpolator",
  //"-private-shadow",
  //"-poly-implicit-overload",
  //"-option-implicit",
  //"-delayedinit-select",
  //"-by-name-right-associative",
  //"-package-object-classes",
  //"-unsound-match",
  "-Xcheckinit",                    //Wrap field accessors to throw an exception on uninitialized access.
  "-Xlog-reflective-calls",         //Print a message when a reflective method call is generated
  "-Xno-forwarders",                //Do not generate static forwarders in mirror classes.
  "-Xno-uescape",                   //Disable handling of \ u unicode escapes.
  "-Ybreak-cycles",                 //Attempt to break cycles encountered during typing
//  "-Yclosure-elim",                 //Perform closure elimination.
//  "-Yconst-opt",                    //Perform optimization with constant values.
//  "-Ydead-code",                    //Perform dead code elimination.
  "-Yinfer-argument-types",         //Infer types for arguments of overridden methods.
  "-Ypresentation-strict",          //Do not report type errors in sources with syntax errors.
  "-Ywarn-dead-code",               //Warn when dead code is identified.
  "-Ywarn-inaccessible",            //Warn about inaccessible types in method signatures.
  "-Ywarn-infer-any",               //Warn when a type argument is inferred to be `Any`.
  "-Ywarn-nullary-override",        //Warn when non-nullary `def f()' overrides nullary `def f'.
  "-Ywarn-nullary-unit",            //Warn when nullary methods return Unit.
  "-Ypos-debug",                    //Trace position validation.

  "-deprecation",
  "-unchecked",
  "-feature"
)

enablePlugins(ScalaNativePlugin)